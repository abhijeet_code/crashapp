package com.example.crashapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends Activity implements OnItemSelectedListener {

	private EditText editText;
	private Spinner dropDown;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		editText = (EditText) findViewById(R.id.edittext);
		dropDown = (Spinner) findViewById(R.id.dropDown);
		dropDown.setOnItemSelectedListener(this);
	}

	private void goToLastActivity(String textBox, String selected) {
		Intent lastActivity = new Intent(this, LastActivity.class);
		lastActivity.putExtra("EditText", textBox);
		lastActivity.putExtra("DropDown", selected);
		startActivity(lastActivity);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
		String selected = parent.getItemAtPosition(pos).toString();
		String textBox = editText.getText().toString();
		Log.i("example", "Selected " + selected);
		goToLastActivity(textBox, selected);

	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub

	}

}
