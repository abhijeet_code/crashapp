package com.example.crashapp;

import java.util.Iterator;
import java.util.Set;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LastActivity extends Activity {

	private static final String LOG_TAG = "example";
	private Button button;
	private EditText showText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.last);

		final String value1 = getIntent().getExtras().getString("EditText");
		final String value2 = getIntent().getExtras().getString("DropDown");
		showText = (EditText) findViewById(R.id.showText);
		showText.setText("EditText : " + value1 + "\nDropDown : " + value2);

		dumpIntent(getIntent());

		button = (Button) findViewById(R.id.button);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (value2.equals("2")) {
					throw new RuntimeException();
				}
			}

		});

	}

	public static void dumpIntent(Intent i) {

		Bundle bundle = i.getExtras();
		if (bundle != null) {
			Set<String> keys = bundle.keySet();
			Iterator<String> it = keys.iterator();
			Log.e(LOG_TAG, "Dumping Intent start");
			while (it.hasNext()) {
				String key = it.next();
				Log.e(LOG_TAG, "[" + key + "=" + bundle.get(key) + "]");
			}
			Log.e(LOG_TAG, "Dumping Intent end");
		}
	}
}
